import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightsearchFormComponent } from './flightsearch-form.component';

describe('FlightsearchFormComponent', () => {
  let component: FlightsearchFormComponent;
  let fixture: ComponentFixture<FlightsearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightsearchFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightsearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

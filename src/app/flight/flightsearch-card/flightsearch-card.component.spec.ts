import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightsearchCardComponent } from './flightsearch-card.component';

describe('FlightsearchCardComponent', () => {
  let component: FlightsearchCardComponent;
  let fixture: ComponentFixture<FlightsearchCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightsearchCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightsearchCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

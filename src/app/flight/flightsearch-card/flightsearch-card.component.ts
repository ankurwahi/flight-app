import { Component, OnInit,Input } from '@angular/core';
import {PlanetDTO } from '../shared/planet.dto';
@Component({
  selector: 'utu-flightsearch-card',
  templateUrl: './flightsearch-card.component.html',
  styleUrls: ['./flightsearch-card.component.css']
})
export class FlightsearchCardComponent implements OnInit {
  constructor() {
    }
  @Input() public planets:PlanetDTO[] = [];
  @Input() name: string;
  ngOnInit() {
  }
}

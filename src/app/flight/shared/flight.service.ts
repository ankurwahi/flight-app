import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { PlanetDTO } from "./planet.dto";
import { PLANET_URL } from "../../app.endpoint";
import { map, reduce, publishReplay, share } from "rxjs/operators";
import { pipe } from "rxjs";
//import * as moment from 'moment';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class FlightService {
  private headers = new Headers({ "Content-Type": "application/json" });
  private URL_PLANET: string = PLANET_URL;

  constructor(private _http: HttpClient) {}

  public searchPlanet(PlanetDTO: PlanetDTO): Observable<any> {
    debugger;
    return this._http.get(this.URL_PLANET).pipe(
      map((res: Response) => {
        return res.results.filter(data => data.name === PlanetDTO.from);
      }),
      share()
    );
  }
  public getPlanets(): Observable<any> {
    return this._http.get(this.URL_PLANET).pipe(
      map((res: Response) => {
        var response = res.json();
        return response as PlanetDTO;
      }),
      share()
    );
  }
  /* public getPlanets(): Observable<any> {
    return this._http.get(this.URL_PLANET).map((res: Response) => {
      var response = res.json();
      return response as PlanetDTO;
    });
  } */
}

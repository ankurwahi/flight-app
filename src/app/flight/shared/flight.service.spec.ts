import { TestBed, inject } from '@angular/core/testing';

import { TimeCardService } from './timecard.service';

describe('TimecardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimeCardService]
    });
  });

  it('should be created', inject([TimeCardService], (service: TimeCardService) => {
    expect(service).toBeTruthy();
  }));
});

export interface PlanetDTO {
  id:number,
  name:string,
  from:string,
  desc:string,
  population:number,
  flag:string
}

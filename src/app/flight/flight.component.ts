import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {PlanetDTO } from './shared/planet.dto';
import { Router } from '@angular/router';
import { FlightService } from './shared/flight.service';
@Component({
  selector: 'utu-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {
  isSearching:Boolean=false;
  constructor(private flightService:FlightService,
              private router:Router
  ) {
  }
  private planets$:Observable<PlanetDTO>;
  ngOnInit() {
// Get saved data from sessionStorage
    var data = sessionStorage.getItem('user');
    if(data)
    {
      //user authenticated
    }
    else
    {
      //user not authenticated, send it to home
      this.router.navigateByUrl('/login')

    }
  }
  searchHandler(PlanetDTO:PlanetDTO) {
    this.isSearching=true;
    console.log(PlanetDTO);
    this.planets$ = this.flightService.searchPlanet(PlanetDTO);
    this.planets$.subscribe(data => {
      this.isSearching=false;
     });
  }
}

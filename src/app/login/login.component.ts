import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import { UserDTO } from "./shared/user.dto";
import { UserService } from "./shared/user.service";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "utu-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public myForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];
  private users$: Observable<any>;

  constructor(
    private _fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.myForm = this._fb.group({
      user: [new FormControl(), [<any>Validators.required]],
      pass: [new FormControl(), [<any>Validators.required]]
    });

    const _form = {
      user: "",
      pass: ""
    };

    (<FormGroup>this.myForm).setValue(_form, { onlySelf: true });
  }
  save(e, model: UserDTO, isValid: boolean) {
    e.preventDefault();
    this.submitted = true;
    console.log(model, isValid);

    var users = this.userService.login(model);
    if (users) {
      sessionStorage.setItem("user", users);
      this.router.navigateByUrl("/flight/search");
    } else {
      this.snackBar.open("login not success", "please try again", {
        duration: 2000
      });
    }
  }
}

﻿import { InMemoryDbService } from "angular-in-memory-web-api";
import { Injectable } from "@angular/core";

@Injectable()
export class InMemoryDataService implements InMemoryDbService {
  public createDb() {
    const users = [
      { id: 1, user: "Luke Skywalker", pass: "19BBY" },
      { id: 2, user: "test", pass: "test" },
      { id: 3, user: "user", pass: "user" }
    ];

    const planets = [
      {
        id: "1",
        name: "Delhi",
        desc: "Lorem Ipsum Desc",
        population: 100000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg"
      },
      {
        id: "2",
        name: "Delhi",
        desc: "Lorem Ipsum Desc",
        population: 10000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg"
      },
      {
        id: "3",
        name: "Mumbai",
        desc: "Lorem Ipsum Desc",
        population: 20000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg"
      },
      {
        id: "4",
        name: "Chandigarh",
        desc: "Lorem Ipsum Desc",
        population: 30000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg"
      },
      {
        id: "5",
        name: "Bhubneshwar",
        desc: "Lorem Ipsum Desc",
        population: 50000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg"
      },
      {
        id: "6",
        name: "Ahmedabad",
        desc: "Lorem Ipsum Desc",
        population: 60000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg"
      },
      {
        id: "7",
        name: "Chennai",
        desc: "Lorem Ipsum Desc",
        population: 70000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg"
      },
      {
        id: "8",
        name: "Banglore",
        desc: "Lorem Ipsum Desc",
        population: 80000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg"
      },
      {
        id: "9",
        name: "Hyderabad",
        desc: "Lorem Ipsum Desc",
        population: 90000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg"
      },
      {
        id: "10",
        name: "Amritsar",
        desc: "Lorem Ipsum Desc",
        population: 100000,
        flag:
          "https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg"
      }
    ];

    return {
      users,
      planets
    };
  }

  public responseInterceptor(responseOptions: any, requestInfo: any): any {
    if (responseOptions.body) {
      let responseBody = responseOptions.body.data;
      responseOptions.body = responseBody;
    }
    return responseOptions;
  }
}
